####################################################################################################
# web service python
####################################################################################################
# vai su terminale e posizionati sulla cartella contenente start.py
# esegui python start.py
# vai su browser e esegui http://localhost:5000/

# Per vedere i file modificati non salvati su cloud
# - git status

# Per salvare un singolo file su cloud
# -git add .
# -git commit -m "my new files" start.py
# -git push

# Per caricare i file su cloud vai su prompt e digita
# -git add .
# -git commit -m "my new files" .
# -git push

# Vado su prompt e digito:
# -set HOST = localhost
# -set PORT = 8080

# Infine digito:
# -python start.py

# Vedere virtual env; a seconda del virtual env che scelgo vengono caricate le librerie giuste
# dependencies: Importante dichiarare dipendenze e rendere la app indipendende così si può replicare su altre
# macchine e scalare (sono in requirements)
# Config: nessuna cosa statica all'interno del codice, solo parametri, e poi aggiungere
# file di configurazione. I parametri sono variabili d'ambiente nel file configurazione.
# In gitignore mettiamo espressioni regolari che indicano i file da escludere durante l'upload
# su cloud
# Backing service: ip, porta, username, password per collegarsi al servizio mysql ecc
# build, release, run (sono gli stage, la pipeline per deployare la app): in build faccio il build
# in parallelo di diversi job 
####################################################################################################

import os
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello world!'

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    host = os.environ.get("HOST", "0.0.0.0")
    app.run(host = host, port = port)

